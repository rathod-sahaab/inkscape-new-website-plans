# Inkscape new website features

## Reference:

- [Blender's site](blender.org)

---

- Pages

  - Homepage

    - First impression `section`

      - Art
      - Tagline
      - Short description (FOSS vector graphics)
      - Download link/button (Auto determine OS, can also select manually)

    - News section
    - Feature flaunts `sections`
      - Some unique features like LPE, short gifs to demonstrate
      - More features
    - Stress on FOSS and donate `section` and our sponsors
    - Footer

  - Support `page`

    - What will your donation achieve
    - How to donate
    - Our sponsors

  - Get Involved `page`

    - Develop
    - Contribute

  - Community `page`

    - IRC, rocketchat links
    - Gitlab links
    - Posts, pages, users, arts(gallery)

- Learn `page`

- Features

  - Posts

    - Users can post art and short texts (like instagram and twitter)
      - Like posts
      - Comment
      - share
      - Mention in a comment (reddit like)
        - users
        - embed Another comment
        - Embed another post
    - Post can be expanded to feel like a blog (or say blog can be compressed to a post compatible preview)
    - Groups: users can create dedicated groups for example
      - Art-form groups (lineart, material, icons, mockups etc.)
      - Regional groups (India, UK, US etc.)
    - Follow Groups and Users
